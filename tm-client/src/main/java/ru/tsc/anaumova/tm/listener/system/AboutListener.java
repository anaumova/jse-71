package ru.tsc.anaumova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.event.ConsoleEvent;

@Component
public final class AboutListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@aboutListener.getName() == #event.name || @aboutListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        @NotNull final String authorName = getPropertyService().getAuthorName();
        @NotNull final String authorEmail = getPropertyService().getAuthorEmail();
        System.out.println("Name: " + authorName);
        System.out.println("E-mail: " + authorEmail);
    }

}