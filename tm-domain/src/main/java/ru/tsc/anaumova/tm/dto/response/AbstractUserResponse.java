package ru.tsc.anaumova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.dto.model.UserDto;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDto user;

    public AbstractUserResponse(@Nullable UserDto user) {
        this.user = user;
    }

}